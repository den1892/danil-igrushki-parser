<?php


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder){
    $containerBuilder->setParameter('app.img_water_mark_dir', '%kernel.project_dir%/files/env/parser/img_water_mark');
};
