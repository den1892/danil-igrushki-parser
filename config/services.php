<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;


use App\Component\Gd\FileAlreadyExistException;
use App\Component\ParserManager\Exception\FileWasNotUploadedException;
use App\Component\ParserManager\Model\ImgInfo;
use App\Component\ParserManager\ParserManager;
use App\Component\ParserManager\Model\ProductModelToSave;
use App\Component\ParserManager\Exception\ProductsNotLoadedException;
use App\Component\ProductFileParser\CsvFileParser;
use App\Component\ProductFileParser\FileNotFoundException;
use App\Component\ProductFileParser\ParserFileFactory;
use App\Component\ProductFileParser\XlsFileFileParser;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\PropertyAccess\PropertyAccessor;

$parameters = [
    'logo_file_path' => '%kernel.project_dir%/files/logo.png',
    'file_path_igrushki' => '%kernel.project_dir%/files/env/igrushki.csv',
    'file_path_export_opt' => '%kernel.project_dir%/files/env/export-opt-club.xlsx',
    'img_water_mark_dir' => $_ENV['IMG_WATER_MARK_DIR']
];

function buildContainer(ContainerBuilder $cb)
{
    $cb
        ->autowire(Filesystem::class);
    $cb
        ->autowire(PropertyAccessor::class);


    $cb
        ->register('parser_file.igrushki', CsvFileParser::class)
        ->setFactory([new Reference(ParserFileFactory::class), 'makeIgrushki7Parser']);
    $cb
        ->register('parser_file.export_opt', XlsFileFileParser::class)
        ->setFactory([new Reference(ParserFileFactory::class), 'makeExportOptParser']);

    $cb
        ->autowire(ParserManager::class)
        ->setArgument('$dir', '%kernel.project_dir%/files/env/parser')
        ->setArgument('$imgWaterMarkDir', '%app.img_water_mark_dir%')
        ->addMethodCall('addFileLink', [
            'http://igrushki7.ua/export/prom_csv.csv',
            new Reference('parser_file.igrushki')
        ])
        ->addMethodCall('addFileLink', [
            'http://optclub.com.ua/upload/exportoptclub.xlsx',
            new Reference('parser_file.export_opt')
        ])
    ;


}

$bindDefault = [
    'logo_file_path' => '%app.logo_file_path%'
];


$excludeDirs = [
    'Model'
];

$excludeClasses = [
    FileNotFoundException::class,
    FileAlreadyExistException::class,
    FileWasNotUploadedException::class,
    ImgInfo::class,
    ProductModelToSave::class,
    ProductsNotLoadedException::class
];

return function (ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder) use ($parameters, $excludeDirs, $excludeClasses, $bindDefault) {


    foreach ($parameters as $parameter => $value) {
        $containerBuilder->setParameter('app.' . $parameter, $value);
    }

    $services = $containerConfigurator->services();
    $servicesDefaults = $services->defaults()->autowire()->autoconfigure();

    foreach ($bindDefault as $varName => $value) {

        $varRealName = str_replace('_', '', ucwords($varName, '_'));
        $varRealName = lcfirst($varRealName);

        $servicesDefaults->bind('$' . $varRealName, $value);
    }

    $exclude = 'DependencyInjection,Entity,Migrations,Tests,Kernel.php';
    foreach ($excludeClasses as $class) {
        $exclude .= ',' . str_replace(['App\\', '\\'], ['', '/'], $class) . '.php';
    }
    if ($excludeDirs) {
        $exclude .= ',' . implode(',', $excludeDirs);
    }
    $services
        ->load('App\\', '../src/*')
        ->lazy()
        ->exclude('../src/{' . $exclude . '}');

    buildContainer($containerBuilder);

};
