<?php


namespace App\Model;


class ProductModel
{

    const OPTION_FIELDS = [
        'weight' => 'Вес',
        'length' => 'Длина',
        'width' => 'Ширина',
        'height' => 'Высота',
        'packaging' => 'Упаковка',
        'packingHeight' => 'Высота упаковки'
    ];

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $categoryName;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var float|null
     */
    public $weight;

    /**
     * @var float|null
     */
    public $length;

    /**
     * @var float|null
     */
    public $width;

    /**
     * @var float|null
     */
    public $height;

    /**
     * @var string|null
     */
    public $packaging;

    /**
     * @var float|null
     */
    public $packingLength;

    /**
     * @var float|null
     */
    public $packingWidth;

    /**
     * @var float|null
     */
    public $packingHeight;

    /**
     * @var float|null
     */
    public $price;

    /**
     * @var string|null
     */
    public $img;

    /**
     * @var bool
     */
    public $inStock;

    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $vendorName;

    /**
     * @var string|null
     */
    public $article;


    /**
     * @return array
     */
    public function getOptions(): array
    {
        foreach (self::OPTION_FIELDS as $property => $name){
            $value = $this->$property;
            if ((is_string($value) && trim($value)) || is_float($value) || is_integer($value)){
                $result[$name] = $value;
            }
        }
        return $result ?? [];
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return (float)str_replace([' ', ','], ['', '.'], $this->price);
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->inStock === '-' ? 0 : 999;
    }

    public function setNotInStock(): self
    {
        $this->inStock = '-';

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @return string|null
     */
    public function getPackaging(): ?string
    {
        return $this->packaging;
    }

    /**
     * @return float|null
     */
    public function getPackingLength(): ?float
    {
        return $this->packingLength;
    }

    /**
     * @return float|null
     */
    public function getPackingWidth(): ?float
    {
        return $this->packingWidth;
    }

    /**
     * @return float|null
     */
    public function getPackingHeight(): ?float
    {
        return $this->packingHeight;
    }

    /**
     * @return string|null
     */
    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getVendorName(): ?string
    {
        return $this->vendorName;
    }

    /**
     * @return string|null
     */
    public function getArticle(): ?string
    {
        return $this->article;
    }

}
