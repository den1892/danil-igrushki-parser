<?php


namespace App\Repository;


use App\Entity\Collection\SsProductCollection;
use App\Entity\SsProducts;
use App\Repository\Filter\SsProductFilter;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SsProductRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($registry,SsProducts::class);
    }

    /**
     * @param SsProductFilter|null $filter
     * @return SsProductCollection
     */
    public function findByFilter(?SsProductFilter $filter = null): SsProductCollection
    {
        $qb = $this->createQueryBuilder('p');

        if ($filter){
            if ($filter->getCategoryIdMin()){
                $qb->andWhere('p.categoryid > :p1')->setParameter('p1', $filter->getCategoryIdMin());
            }
        }
        return new SsProductCollection($qb->getQuery()->getResult());
    }

}
