<?php


namespace App\Component\Gd;


use Throwable;

class UnknownFileExtensionException extends \Exception
{

    public function __construct(string $filePath = '/*/', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Unknown '.$filePath.' file extension.', $code, $previous);
    }

}
