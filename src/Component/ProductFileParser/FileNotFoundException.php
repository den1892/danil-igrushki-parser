<?php


namespace App\Component\ProductFileParser;


use Throwable;

class FileNotFoundException extends \Exception
{
    public function __construct(?string $filePath, $code = 0, Throwable $previous = null)
    {
        parent::__construct('File '.$filePath.' not found', $code, $previous);
    }
}
