<?php


namespace App\Component\DownloadFile;


interface DownloadFileInterface
{

    /**
     * @param string $url
     * @param string $filePath
     * @return \SplFileInfo
     */
    public function download(string $url, string $filePath): \SplFileInfo;

}
