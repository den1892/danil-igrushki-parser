<?php


namespace App\Component\DownloadFile;


use Symfony\Component\Filesystem\Filesystem;

class DownloadFile implements DownloadFileInterface
{

    /**
     * @var Filesystem
     */
    private $fileSystem;
    /**
     * @var string
     */
    private $dir;

    public function __construct(string $dir = null)
    {
       // $this->fileSystem = new Filesystem();
        $this->dir = $dir;
    }

    /**
     * @inheritDoc
     */
    public function download(string $url, string $filePath): \SplFileInfo
    {
        set_time_limit(0);
        $fp = fopen($filePath, 'w+');
        $ch = curl_init(str_replace(" ", "%20", $url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

//        if (!is_file($fileName)) {
//            file_put_contents($fileName, file_get_contents($url));
//        }
        return new \SplFileInfo($filePath);
    }
//
//    /**
//     * @param string $url
//     * @param string|null $name
//     * @return string
//     */
//    private function createFilePath(string $url, ?string $name = null): string
//    {
//        if (!$name) {
//            $name = basename($url);
//        }
//
//        $filePath = $this->dir.'/'.$name;
//
//        $dir = dirname($filePath);
//
//        if (!is_dir($dir)) {
//            $this->fileSystem->mkdir($dir);
//        }
//        return $filePath;
//    }

}
