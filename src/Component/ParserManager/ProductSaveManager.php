<?php


namespace App\Component\ParserManager;


use App\Component\ParserManager\Finder\BrandFinder;
use App\Component\ParserManager\Finder\CategoryFinder;
use App\Component\ParserManager\Finder\OptionFinder;
use App\Component\ParserManager\Model\ProductModelToSaveCollection;
use App\Entity\SsProductOptionsValues;
use App\Entity\SsProducts;
use Doctrine\ORM\EntityManagerInterface;

class ProductSaveManager
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BrandFinder
     */
    private $brandFinder;
    /**
     * @var CategoryFinder
     */
    private $categoryFinder;
    /**
     * @var OptionFinder
     */
    private $optionFinder;

    /**
     * @var SsProducts[]
     */
    private $products;
    /**
     * @var ProductModelToEntity
     */
    private $productModelToEntity;

    public function __construct(EntityManagerInterface $entityManager, ProductModelToEntity $productModelToEntity, BrandFinder $brandFinder, CategoryFinder $categoryFinder, OptionFinder $optionFinder)
    {
        $this->entityManager = $entityManager;
        $this->brandFinder = $brandFinder;
        $this->categoryFinder = $categoryFinder;
        $this->optionFinder = $optionFinder;

        $products = $this->entityManager->getRepository(SsProducts::class)->findAll();
        foreach ($products as $product){
            $this->products[$product->getName()] = $product;
        }
        $this->productModelToEntity = $productModelToEntity;
    }

    /**
     * @param ProductModelToSaveCollection $productModelToSaveCollection
     */
    public function update(ProductModelToSaveCollection $productModelToSaveCollection): void
    {
        foreach ($productModelToSaveCollection as $productModelToSave){

            $productName = $productModelToSave->getProductModel()->getName();

            if (isset($this->products[$productName])){
                $product = $this->products[$productName];
                $this->productModelToEntity->update($productModelToSave->getProductModel(), $product);

//                if (!$product->getVariants()->count()){
//                    $variants = $this->optionFinder->findVariants($productModelToSave->getProductModel());
//                    foreach ($variants as $variant){
//                        $var = (new SsProductOptionsValues)->setOption($variant->getOption())->setVariant($variant)->setProduct($product);
//                        $this->entityManager->persist($var);
//                    }
//                }

            }
        }
    }

    /**
     * @param ProductModelToSaveCollection $productModelToSaveCollection
     */
    public function create(ProductModelToSaveCollection $productModelToSaveCollection): void
    {
        foreach ($productModelToSaveCollection as $productModelToSave){

            $productName = $productModelToSave->getProductModel()->getName();
            if (isset($this->products[$productName])){
                continue;
            }

            $brand = $this->brandFinder->findOrCreate($productModelToSave->getProductModel());
            $category = $this->categoryFinder->findOrCreate($productModelToSave->getProductModel());
            $variants = $this->optionFinder->findVariants($productModelToSave->getProductModel());

            $product = $this->productModelToEntity->create($productModelToSave)->setBrand($brand)->setCategory($category);
            $this->entityManager->persist($product);

            $this->products[$productName] = $product;

//            foreach ($variants as $variant){
//                $var = (new SsProductOptionsValues)->setOption($variant->getOption())->setVariant($variant)->setProduct($product);
//                $this->entityManager->persist($var);
//            }

        }
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }

}
