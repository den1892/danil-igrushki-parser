<?php


namespace App\Component\ParserManager\Model;


use App\Component\ParserManager\Model\ProductModelToSave;

class ProductModelToSaveCollection implements \Iterator
{

    /**
     * @var ProductModelToSave
     */
    protected $elements = [];

    protected $position = 0;

    public function __construct(array $array = [])
    {
        $this->elements = $array;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current(): ProductModelToSave
    {
        return $this->elements[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->elements[$this->position]);
    }

    /**
     * @param ProductModelToSave $entity
     */
    public function add(ProductModelToSave $entity)
    {
        $this->elements[] = $entity;
    }

    /**
     * @return array|null
     */
    public function getArr(): array
    {
        return $this->elements;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function exist($entity)
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        return true;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function remove($entity): bool
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        unset($this->elements[$key]);
        $this->elements = array_values($this->elements);
        return true;
    }

}
