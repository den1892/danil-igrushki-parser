<?php


namespace App\Component\ParserManager\Model;


use App\Component\ParserManager\Model\ImgInfo;
use App\Model\ProductModel;

class ProductModelToSave
{

    /**
     * @var ProductModel
     */
    private $productModel;

    /**
     * @var ImgInfo|null
     */
    private $imgInfo;

    public function __construct(ProductModel $productModel, ?ImgInfo $imgInfo = null)
    {
        $this->productModel = $productModel;
        $this->imgInfo = $imgInfo;
    }

    /**
     * @return ProductModel
     */
    public function getProductModel(): ProductModel
    {
        return $this->productModel;
    }

    /**
     * @return ImgInfo|null
     */
    public function getImgInfo(): ?ImgInfo
    {
        return $this->imgInfo;
    }

}
