<?php


namespace App\Component\ParserManager\Model;


class ImgInfo
{

    /**
     * @var \SplFileInfo
     */
    private $m;

    private $b;

    private $s;

    private $sc;

    private $h;

    /**
     * ImgInfo constructor.
     * @param \SplFileInfo $img
     */
    public function __construct(\SplFileInfo $img)
    {
        $this->m = $img;
        $this->b = $this->createFileWithSuffix('-B');
        $this->s = $this->createFileWithSuffix('-S');
        $this->sc = $this->createFileWithSuffix('-SC');
        $this->h = $this->createFileWithSuffix('-H');
    }

    /**
     * @return \SplFileInfo
     */
    public function getM(): \SplFileInfo
    {
        return $this->m;
    }

    /**
     * @return \SplFileInfo
     */
    public function getB(): \SplFileInfo
    {
        return $this->b;
    }

    /**
     * @return \SplFileInfo
     */
    public function getS(): \SplFileInfo
    {
        return $this->s;
    }

    /**
     * @return \SplFileInfo
     */
    public function getSc(): \SplFileInfo
    {
        return $this->sc;
    }

    /**
     * @return \SplFileInfo
     */
    public function getH(): \SplFileInfo
    {
        return $this->h;
    }

    /**
     * @param string $suffix
     * @return \SplFileInfo
     */
    private function createFileWithSuffix(string $suffix): \SplFileInfo
    {
        return $this->m;
        $ext = '.' . $this->m->getExtension();
        $filePath = $this->m->getPath() . '/' . $this->m->getBasename($ext) . $suffix . $ext;

        return new \SplFileInfo($filePath);
    }

}
