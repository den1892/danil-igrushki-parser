<?php


namespace App\Component\Watermark;


interface WatermarkInterface
{

    /**
     * @param string $filePath
     * @param string|null $filePathTo
     */
    public function set(string $filePath, ?string $filePathTo = null);

}
