<?php


namespace App\Component\Watermark;


use App\Component\Gd\Gd;
use App\Component\Gd\UnknownFileExtensionException;

class Watermark implements WatermarkInterface
{

    /**
     * @var string
     */
    private $logoFilePath;

    /**
     * @var resource
     */
    private $logoResource;
    /**
     * @var Gd
     */
    private $gd;

    public function __construct(string $logoFilePath, Gd $gd)
    {
        $this->logoFilePath = $logoFilePath;
        $this->gd = $gd;
    }

    /**
     * @param string $filePath
     * @param string|null $filePathTo
     * @param bool $overwrite
     * @throws UnknownFileExtensionException
     */
    public function set(string $filePath, ?string $filePathTo = null, bool $overwrite = false)
    {
        $filePathTo = $filePathTo ?? $filePath;

        if (!file_exists($filePathTo) || true === $overwrite) {

            try {
                $image = $this->gd->createImageFrom($filePath);
                $wImage = imagesx($image);
                $hImage = imagesy($image);

                $logo = $this->logoResource($wImage * 0.4, $hImage * 0.4);

                $wLogo = imagesx($logo);
                $hLogo = imagesy($logo);


                [$x, $y] = [$wImage - $wLogo - 10, $hImage - $hLogo - 10];
                imagecopymerge($image, $logo, $x, $y, 0, 0, $wLogo, $hLogo, 30);

                $this->gd->saveImage($image, $filePathTo ?? $filePath);
                imagedestroy($image);
            } catch (UnknownFileExtensionException $exception) {
                throw $exception;
            } catch (\Exception $exception) {
                return;
            }

        }
    }

    /**
     * @param float $max_width
     * @param float $max_height
     * @return false|resource
     * @throws UnknownFileExtensionException
     */
    private function logoResource(float $max_width, float $max_height)
    {
        if (null === $this->logoResource) {
            $this->logoResource = $this->gd->createImageFrom($this->logoFilePath);
        }

        $orig_width = imagesx($this->logoResource);
        $orig_height = imagesy($this->logoResource);

        $width = $orig_width;
        $height = $orig_height;

        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        $image_p = imagecreatetruecolor($width, $height);


        imagecopyresampled($image_p, $this->logoResource, 0, 0, 0, 0,
            $width, $height, $orig_width, $orig_height);

        return $image_p;
    }
}
