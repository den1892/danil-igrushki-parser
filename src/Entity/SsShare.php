<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsShare
 *
 * @ORM\Table(name="SS_share")
 * @ORM\Entity
 */
class SsShare
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=25, nullable=true)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type_val", type="integer", nullable=true)
     */
    private $typeVal;

    /**
     * @var float|null
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=true)
     */
    private $value;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=150, nullable=true)
     */
    private $code;

    /**
     * @var bool
     *
     * @ORM\Column(name="default", type="boolean", nullable=false)
     */
    private $default = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeVal(): ?int
    {
        return $this->typeVal;
    }

    public function setTypeVal(?int $typeVal): self
    {
        $this->typeVal = $typeVal;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDefault(): ?bool
    {
        return $this->default;
    }

    public function setDefault(bool $default): self
    {
        $this->default = $default;

        return $this;
    }


}
