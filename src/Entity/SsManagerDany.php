<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsManagerDany
 *
 * @ORM\Table(name="SS_manager_dany")
 * @ORM\Entity
 */
class SsManagerDany
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_manager", type="integer", nullable=true)
     */
    private $idManager;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dpt", type="text", length=65535, nullable=true)
     */
    private $dpt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdManager(): ?int
    {
        return $this->idManager;
    }

    public function setIdManager(?int $idManager): self
    {
        $this->idManager = $idManager;

        return $this;
    }

    public function getDpt(): ?string
    {
        return $this->dpt;
    }

    public function setDpt(?string $dpt): self
    {
        $this->dpt = $dpt;

        return $this;
    }


}
