<?php


namespace App\Entity\Collection;


use App\Entity\SsProducts;

class SsProductCollection extends EntityCollection
{

    /**
     * @return SsProducts
     */
    public function current(): SsProducts
    {
        return parent::current();
    }

}
