<?php


namespace App\Entity\Collection;


use Symfony\Component\PropertyAccess\PropertyAccessor;

class EntityCollection implements \Iterator
{


    protected $elements = [];

    protected $position;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    public function __construct(array $array = [])
    {
        $this->position = 0;
        $this->elements = $array;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->elements[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->elements[$this->position]);
    }

    public function add($entity)
    {
        $this->elements[] = $entity;
    }

    /**
     * @return array|null
     */
    public function getArr(): array
    {
        return $this->elements;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function exist($entity)
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        return true;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function remove($entity): bool
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        unset($this->elements[$key]);
        $this->elements = array_values($this->elements);
        return true;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->elements);
    }

    /***
     * @return int
     */
    public function ceil(): int
    {
        return ceil($this->count());
    }


    /**
     * @return PropertyAccessor
     */
    protected function getPropertyAccessor(): PropertyAccessor
    {
        if (null === $this->propertyAccessor) {
            $this->propertyAccessor = new PropertyAccessor();
        }
        return $this->propertyAccessor;
    }
}
