<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsPayoption
 *
 * @ORM\Table(name="SS_payoption")
 * @ORM\Entity
 */
class SsPayoption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="payID", type="integer", nullable=true)
     */
    private $payid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payoption", type="string", length=255, nullable=true)
     */
    private $payoption;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payvalue", type="string", length=255, nullable=true)
     */
    private $payvalue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayid(): ?int
    {
        return $this->payid;
    }

    public function setPayid(?int $payid): self
    {
        $this->payid = $payid;

        return $this;
    }

    public function getPayoption(): ?string
    {
        return $this->payoption;
    }

    public function setPayoption(?string $payoption): self
    {
        $this->payoption = $payoption;

        return $this;
    }

    public function getPayvalue(): ?string
    {
        return $this->payvalue;
    }

    public function setPayvalue(?string $payvalue): self
    {
        $this->payvalue = $payvalue;

        return $this;
    }


}
