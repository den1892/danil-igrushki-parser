<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsCurrencyTypes
 *
 * @ORM\Table(name="SS_currency_types")
 * @ORM\Entity
 */
class SsCurrencyTypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="CID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Name", type="string", length=30, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=7, nullable=true)
     */
    private $code;

    /**
     * @var float|null
     *
     * @ORM\Column(name="currency_value", type="float", precision=10, scale=0, nullable=true)
     */
    private $currencyValue;

    /**
     * @var int|null
     *
     * @ORM\Column(name="where2show", type="integer", nullable=true)
     */
    private $where2show;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="currency_iso_3", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $currencyIso3;

    public function getCid(): ?int
    {
        return $this->cid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCurrencyValue(): ?float
    {
        return $this->currencyValue;
    }

    public function setCurrencyValue(?float $currencyValue): self
    {
        $this->currencyValue = $currencyValue;

        return $this;
    }

    public function getWhere2show(): ?int
    {
        return $this->where2show;
    }

    public function setWhere2show(?int $where2show): self
    {
        $this->where2show = $where2show;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getCurrencyIso3(): ?string
    {
        return $this->currencyIso3;
    }

    public function setCurrencyIso3(?string $currencyIso3): self
    {
        $this->currencyIso3 = $currencyIso3;

        return $this;
    }


}
