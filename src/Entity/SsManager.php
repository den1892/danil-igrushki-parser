<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsManager
 *
 * @ORM\Table(name="SS_manager")
 * @ORM\Entity
 */
class SsManager
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="manager", type="string", length=255, nullable=true)
     */
    private $manager;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var int|null
     *
     * @ORM\Column(name="access", type="integer", nullable=true)
     */
    private $access;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @ORM\Column(name="online_type", type="integer", nullable=true)
     */
    private $onlineType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="online_num", type="string", length=255, nullable=true)
     */
    private $onlineNum;

    /**
     * @var string|null
     *
     * @ORM\Column(name="online_name", type="string", length=255, nullable=true)
     */
    private $onlineName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getManager(): ?string
    {
        return $this->manager;
    }

    public function setManager(?string $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAccess(): ?int
    {
        return $this->access;
    }

    public function setAccess(?int $access): self
    {
        $this->access = $access;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getOnlineType(): ?int
    {
        return $this->onlineType;
    }

    public function setOnlineType(?int $onlineType): self
    {
        $this->onlineType = $onlineType;

        return $this;
    }

    public function getOnlineNum(): ?string
    {
        return $this->onlineNum;
    }

    public function setOnlineNum(?string $onlineNum): self
    {
        $this->onlineNum = $onlineNum;

        return $this;
    }

    public function getOnlineName(): ?string
    {
        return $this->onlineName;
    }

    public function setOnlineName(?string $onlineName): self
    {
        $this->onlineName = $onlineName;

        return $this;
    }


}
