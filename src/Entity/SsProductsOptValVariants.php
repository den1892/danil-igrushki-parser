<?php

namespace App\Entity;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Proxy\Proxy;

/**
 * SsProductsOptValVariants
 *
 * @ORM\Table(name="SS_products_opt_val_variants")
 * @ORM\Entity
 */
class SsProductsOptValVariants
{

    /**
     * @ORM\ManyToOne(targetEntity="SsProductOptions")
     * @ORM\JoinColumn(name="optionID", referencedColumnName="optionID")
     */
    private $option;

    /**
     * @var int
     *
     * @ORM\Column(name="variantID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $variantid;

    /**
     * @var int
     *
     * @ORM\Column(name="optionID", type="integer", nullable=false)
     */
    private $optionid=0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder = '0';

    public function getVariantid(): ?int
    {
        return $this->variantid;
    }

    public function getOptionid(): ?int
    {
        return $this->optionid ?? 0;
    }

    public function setOptionid(int $optionid): self
    {
        $this->optionid = $optionid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getOptionName(): ?string
    {
        return null !== $this->getOption() ? $this->getOption()->getName() : null;
    }

    public function getOption(): ?SsProductOptions
    {
        if ($this->option instanceof Proxy){
            try {
                $this->option->__load();
            } catch (EntityNotFoundException $exception){
                $this->option = null;
            }
        }
       return $this->option;
    }

    public function setOption(?SsProductOptions $option): self
    {
        $this->option = $option;

        return $this;
    }


}
