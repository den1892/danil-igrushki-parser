<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsProductOptionsValues
 *
 * @ORM\Table(name="SS_product_options_values")
 * @ORM\Entity
 */
class SsProductOptionsValues
{

    /**
     * @ORM\ManyToOne(targetEntity="SsProductOptions")
     * @ORM\JoinColumn(name="optionID", referencedColumnName="optionID")
     */
    private $option;

    /**
     * @ORM\ManyToOne(targetEntity="SsProductsOptValVariants")
     * @ORM\JoinColumn(name="variantID", referencedColumnName="variantID")
     */
    private $variant;

    /**
     * @ORM\ManyToOne(targetEntity="SsProducts")
     * @ORM\JoinColumn(name="productID", referencedColumnName="productID")
     */
    private $product;



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="variantID", type="integer", nullable=false)
     */
    private $variantid;

    /**
     * @var int
     *
     * @ORM\Column(name="productID", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var int
     *
     * @ORM\Column(name="optionID", type="integer", nullable=false)
     */
    private $optionid;

    /**
     * @var float
     *
     * @ORM\Column(name="price_surplus", type="float", precision=10, scale=0, nullable=false)
     */
    private $priceSurplus = '0';


    /**
     * @var string|null
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @var int|null
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVariantid(): ?int
    {
        return $this->variantid;
    }

    public function setVariantid(int $variantid): self
    {
        $this->variantid = $variantid;

        return $this;
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function setProductid(int $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getOptionid(): ?int
    {
        return $this->optionid;
    }

    public function setOptionid(int $optionid): self
    {
        $this->optionid = $optionid;

        return $this;
    }

    public function getPriceSurplus(): ?float
    {
        return $this->priceSurplus;
    }

    public function setPriceSurplus(float $priceSurplus): self
    {
        $this->priceSurplus = $priceSurplus;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getOption(): ?SsProductOptions
    {
        return $this->option;
    }

    public function setOption(?SsProductOptions $option): self
    {
        $this->option = $option;

        return $this;
    }

    public function getVariant(): ?SsProductsOptValVariants
    {
        return $this->variant;
    }

    public function setVariant(?SsProductsOptValVariants $variant): self
    {
        $this->variant = $variant;

        return $this;
    }

    public function getProduct(): ?SsProducts
    {
        return $this->product;
    }

    public function setProduct(?SsProducts $product): self
    {
        $this->product = $product;

        return $this;
    }


}
