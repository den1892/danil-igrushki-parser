<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsOrdersCarts
 *
 * @ORM\Table(name="SS_orders_carts", indexes={@ORM\Index(name="orderID", columns={"orderID"})})
 * @ORM\Entity
 */
class SsOrdersCarts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="productID", type="string", length=20, nullable=false)
     */
    private $productid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true, options={"fixed"=true})
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var float|null
     *
     * @ORM\Column(name="Quantity", type="float", precision=10, scale=0, nullable=true)
     */
    private $quantity;

    /**
     * @var \SsOrders
     *
     * @ORM\ManyToOne(targetEntity="SsOrders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="orderID", referencedColumnName="orderID")
     * })
     */
    private $orderid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductid(): ?string
    {
        return $this->productid;
    }

    public function setProductid(string $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getOrderid(): ?SsOrders
    {
        return $this->orderid;
    }

    public function setOrderid(?SsOrders $orderid): self
    {
        $this->orderid = $orderid;

        return $this;
    }


}
