<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsCategoryProduct
 *
 * @ORM\Table(name="SS_category_product", indexes={@ORM\Index(name="productID", columns={"productID"}), @ORM\Index(name="categoryID", columns={"categoryID"})})
 * @ORM\Entity
 */
class SsCategoryProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \SsCategories
     *
     * @ORM\ManyToOne(targetEntity="SsCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryID", referencedColumnName="categoryID")
     * })
     */
    private $categoryid;

    /**
     * @var \SsProducts
     *
     * @ORM\ManyToOne(targetEntity="SsProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productID", referencedColumnName="productID")
     * })
     */
    private $productid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryid(): ?SsCategories
    {
        return $this->categoryid;
    }

    public function setCategoryid(?SsCategories $categoryid): self
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    public function getProductid(): ?SsProducts
    {
        return $this->productid;
    }

    public function setProductid(?SsProducts $productid): self
    {
        $this->productid = $productid;

        return $this;
    }


}
