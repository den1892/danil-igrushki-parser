<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsProductOptions
 *
 * @ORM\Table(name="SS_product_options")
 * @ORM\Entity
 */
class SsProductOptions
{
    /**
     * @var int
     *
     * @ORM\Column(name="optionID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $optionid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder = '0';

    public function getOptionid(): ?int
    {
        return $this->optionid;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }


}
