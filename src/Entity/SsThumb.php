<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsThumb
 *
 * @ORM\Table(name="SS_thumb")
 * @ORM\Entity
 */
class SsThumb
{
    /**
     * @var int
     *
     * @ORM\Column(name="thumbID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $thumbid;

    /**
     * @var int
     *
     * @ORM\Column(name="productID", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="picture", type="string", length=150, nullable=true)
     */
    private $picture;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    public function getThumbid(): ?int
    {
        return $this->thumbid;
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function setProductid(int $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


}
