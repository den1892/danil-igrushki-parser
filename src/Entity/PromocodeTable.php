<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PromocodeTable
 *
 * @ORM\Table(name="PROMOCODE_TABLE")
 * @ORM\Entity
 */
class PromocodeTable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=200, nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="discont", type="integer", nullable=false)
     */
    private $discont;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDiscont(): ?int
    {
        return $this->discont;
    }

    public function setDiscont(int $discont): self
    {
        $this->discont = $discont;

        return $this;
    }


}
