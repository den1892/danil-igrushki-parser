<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsCustomers
 *
 * @ORM\Table(name="SS_customers")
 * @ORM\Entity
 */
class SsCustomers
{
    /**
     * @var int
     *
     * @ORM\Column(name="custID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $custid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_password", type="string", length=255, nullable=true)
     */
    private $custPassword;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_email", type="string", length=130, nullable=true)
     */
    private $custEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_firstname", type="string", length=30, nullable=true)
     */
    private $custFirstname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_lastname", type="string", length=30, nullable=true)
     */
    private $custLastname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_country", type="string", length=30, nullable=true)
     */
    private $custCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_zip", type="string", length=30, nullable=true)
     */
    private $custZip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_city", type="string", length=230, nullable=true)
     */
    private $custCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_address", type="string", length=200, nullable=true)
     */
    private $custAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_phone", type="string", length=30, nullable=true)
     */
    private $custPhone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="openID", type="string", length=20, nullable=true)
     */
    private $openid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provider", type="string", length=30, nullable=true)
     */
    private $provider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $date = 'CURRENT_TIMESTAMP';

    public function getCustid(): ?int
    {
        return $this->custid;
    }

    public function getCustPassword(): ?string
    {
        return $this->custPassword;
    }

    public function setCustPassword(?string $custPassword): self
    {
        $this->custPassword = $custPassword;

        return $this;
    }

    public function getCustEmail(): ?string
    {
        return $this->custEmail;
    }

    public function setCustEmail(?string $custEmail): self
    {
        $this->custEmail = $custEmail;

        return $this;
    }

    public function getCustFirstname(): ?string
    {
        return $this->custFirstname;
    }

    public function setCustFirstname(?string $custFirstname): self
    {
        $this->custFirstname = $custFirstname;

        return $this;
    }

    public function getCustLastname(): ?string
    {
        return $this->custLastname;
    }

    public function setCustLastname(?string $custLastname): self
    {
        $this->custLastname = $custLastname;

        return $this;
    }

    public function getCustCountry(): ?string
    {
        return $this->custCountry;
    }

    public function setCustCountry(?string $custCountry): self
    {
        $this->custCountry = $custCountry;

        return $this;
    }

    public function getCustZip(): ?string
    {
        return $this->custZip;
    }

    public function setCustZip(?string $custZip): self
    {
        $this->custZip = $custZip;

        return $this;
    }

    public function getCustCity(): ?string
    {
        return $this->custCity;
    }

    public function setCustCity(?string $custCity): self
    {
        $this->custCity = $custCity;

        return $this;
    }

    public function getCustAddress(): ?string
    {
        return $this->custAddress;
    }

    public function setCustAddress(?string $custAddress): self
    {
        $this->custAddress = $custAddress;

        return $this;
    }

    public function getCustPhone(): ?string
    {
        return $this->custPhone;
    }

    public function setCustPhone(?string $custPhone): self
    {
        $this->custPhone = $custPhone;

        return $this;
    }

    public function getOpenid(): ?string
    {
        return $this->openid;
    }

    public function setOpenid(?string $openid): self
    {
        $this->openid = $openid;

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }


}
