<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsPreoder
 *
 * @ORM\Table(name="SS_preoder", indexes={@ORM\Index(name="preoder_product_id_fk", columns={"product_id"}), @ORM\Index(name="preoder_user_id_fk", columns={"user_id"})})
 * @ORM\Entity
 */
class SsPreoder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="preoder_info", type="string", length=200, nullable=false)
     */
    private $preoderInfo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_up", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createUp = 'CURRENT_TIMESTAMP';

    /**
     * @var \SsProducts
     *
     * @ORM\ManyToOne(targetEntity="SsProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="productID")
     * })
     */
    private $product;

    /**
     * @var \SsCustomers
     *
     * @ORM\ManyToOne(targetEntity="SsCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="custID")
     * })
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPreoderInfo(): ?string
    {
        return $this->preoderInfo;
    }

    public function setPreoderInfo(string $preoderInfo): self
    {
        $this->preoderInfo = $preoderInfo;

        return $this;
    }

    public function getCreateUp(): ?\DateTimeInterface
    {
        return $this->createUp;
    }

    public function setCreateUp(\DateTimeInterface $createUp): self
    {
        $this->createUp = $createUp;

        return $this;
    }

    public function getProduct(): ?SsProducts
    {
        return $this->product;
    }

    public function setProduct(?SsProducts $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getUser(): ?SsCustomers
    {
        return $this->user;
    }

    public function setUser(?SsCustomers $user): self
    {
        $this->user = $user;

        return $this;
    }


}
