<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsSpecialOffers
 *
 * @ORM\Table(name="SS_special_offers")
 * @ORM\Entity
 */
class SsSpecialOffers
{
    /**
     * @var int
     *
     * @ORM\Column(name="offerID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $offerid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="productID", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    public function getOfferid(): ?int
    {
        return $this->offerid;
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function setProductid(?int $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }


}
