<?php


namespace App\Command;


use App\Component\ParserManager\Model\ImgInfo;
use App\Component\ParserManager\ParserManager;
use App\Component\ParserManager\ProductSaveManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParserCommand extends CommandAbstract
{

    protected static $defaultName = 'app:parser';
    /**
     * @var ParserManager
     */
    private $parserManager;
    /**
     * @var ProductSaveManager
     */
    private $productSaveManager;

    public function __construct(ParserManager $parserManager, ProductSaveManager $productSaveManager)
    {
        parent::__construct(null);
        $this->parserManager = $parserManager;
        $this->productSaveManager = $productSaveManager;
    }

    protected function configure()
    {
        $this->addArgument('action', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \App\Component\Gd\UnknownFileExtensionException
     * @throws \App\Component\ParserManager\Exception\FileWasNotUploadedException
     * @throws \App\Component\ParserManager\Exception\ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $i = 0;
        $downloadFileCallBack = function (\SplFileInfo $fileInfo) use ($output, &$i) {
            $i++;
            $this->successMsg($output, $i . ') Download file to ' . $fileInfo->getPathname());
        };

        switch ($input->getArgument('action')) {
            case 'download-files':
            {
                $this->parserManager->downloadFiles($downloadFileCallBack);
                break;
            }
            case 'parser':
            {
                $this->parserManager->parserFiles();
                break;
            }
            case 'download-images':
            {
                $this->parserManager->downloadImages($downloadFileCallBack);
                break;
            }
            case 'set-watermark':
            {
                $i = 0;
                $this->parserManager->setWaterMark(function (ImgInfo $imgInfo) use ($output, &$i) {
                    $i++;
                    $files = [
                        $imgInfo->getM(),
                        $imgInfo->getB(),
                        $imgInfo->getS(),
                        $imgInfo->getSC(),
                        $imgInfo->getH()
                    ];

                    $filesListString = implode(', ', $files);

                    $this->successMsg($output, $i.') Create files in dir '.$imgInfo->getM()->getPath());
                });
                break;
            }
            case 'update':
            {
                $productModels = $this->parserManager->getProductCollectionToSave();
                $this->productSaveManager->update($productModels);
                $this->productSaveManager->flush();
                break;
            }
            case 'create':
            {
                $productModels = $this->parserManager->getProductCollectionToSave();
                $this->productSaveManager->create($productModels);
                $this->productSaveManager->flush();

                break;
            }
            default:
            {
                $this->errorMsg($output, 'Unknown action ' . $input->getArgument('action'));
                return 0;
            }
        }

        $this->successMsg($output);
        return 1;
    }

}
