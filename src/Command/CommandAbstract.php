<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

abstract class CommandAbstract extends Command
{

    /**
     * @param OutputInterface $output
     * @param string $msg
     */
    protected function successMsg(OutputInterface $output, string $msg = 'Success!'): void
    {
        $output->writeln('<fg=green>'.$msg.'</>');
    }

    /**
     * @param OutputInterface $output
     * @param string $msg
     */
    protected function errorMsg(OutputInterface $output, string $msg = 'Error!'): void
    {
        $output->writeln('<fg=red>'.$msg.'</>');
    }

}
