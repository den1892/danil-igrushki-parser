<?php


namespace App\Command;

use App\Component\ParserManager\ParserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppCommand extends CommandAbstract
{
    protected static $defaultName = 'app';
    /**
     * @var ParserManager
     */
    private $parserManager;

    public function __construct(ParserManager $parserManager)
    {
        parent::__construct(null);
        $this->parserManager = $parserManager;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \App\Component\ParserManager\Exception\ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $i = 0;
//        $this->parserManager->downloadImages(function (\SplFileInfo $fileInfo) use ($output, &$i){
//            $i++;
//            $this->successMsg($output, $i.') Download file '.$fileInfo->getPathname());
//        });
        return 1;
    }
}
