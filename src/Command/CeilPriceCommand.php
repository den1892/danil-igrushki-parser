<?php


namespace App\Command;


use App\Entity\SsProducts;
use App\Repository\SsProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CeilPriceCommand extends CommandAbstract
{

    protected static $defaultName = 'app:ceil-price';
    /**
     * @var SsProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(SsProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct(null);
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $products = $this->productRepository->findAll();

        /**
         * @var SsProducts $product
         */
        foreach ($products as $product){
            $product->setPrice(ceil($product->getPrice()));
        }
        $this->entityManager->flush();
        $this->successMsg($output);
        return 1;
    }

}
